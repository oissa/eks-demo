const request = require("supertest");
const assert = require("assert");
const app = require("../index"); // Import the Express app

describe("GET /", () => {
  // a test case that checks if the server is running
  it("respond with 200 OK", (done) => {
    request(app).get("/").expect(200, done);
  });
});
