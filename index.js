const express = require("express");
const path = require("path");
const app = express();
const filePath = path.join(__dirname, "index.html");
const port = 3000;

app.get("/", (req, res) => {
  res.sendFile(filePath);
  // res.send("Welcome to Health Management System!");
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});

module.exports = app; // Export the app for testing
